package database;

import java.io.File;
import java.io.FilenameFilter;

/**
 * Created by Alison on 2016/11/13.
 */
class FileFilter implements FilenameFilter { //实现FilenameFilter接口
    private String fileExtension;

    public FileFilter(String fileExtension) {
        this.fileExtension = fileExtension;
    }

    public boolean accept(File dir, String name) {
        return name.endsWith("." + fileExtension);
    }
}