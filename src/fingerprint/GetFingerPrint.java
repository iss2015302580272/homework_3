package fingerprint;

import database.StoreFingerprint;
import scanner.WaveFileReader;

import java.util.ArrayList;

import static fingerprint.FFT.fft;

/**
 * Created by Alison on 2016/11/19.
 */
public class GetFingerPrint {
    public static ArrayList<ShazamHash> getHashes(String path,String name){
//        String songName = "周杰伦 - 说好的幸福呢_03.wav";
//        String file = "data/wav_part_5/"+songName;
        String file = path+name;

        //Start
        System.out.println("Read into:  "+name+"\n");

        WaveFileReader id_x = new WaveFileReader(file);
        ArrayList<double[]> slices = new ArrayList<double[]>();


        //
        double[] idx = id_x.getFinalData();
        //song_idx是一个fingerprint对象
        FingerPrint song_idx = new FingerPrint(1);
        //计算出划分的块数
        int number = id_x.getDataLen() / FFT.WINDOW_SIZE;
        double[] ret = new double[FFT.WINDOW_SIZE];

        //利用傅里叶算法将时域信息转换成频域信息
        for (int i = 0; i < number; i++) {
            double[] slice = new double[FFT.WINDOW_SIZE];
            for (int j = 0; j < FFT.WINDOW_SIZE; j++) {
                slice[j] = idx[FFT.WINDOW_SIZE * i + j];

            }
            ret = fft(slice);
            //提取出频率的N个最大值
            song_idx.append(fft(slice));
            slices.add(ret);
        }
        //使用组合哈希算法，进行f1、f2、dt的获取
        //hashes是一个傅里叶list
        ArrayList<ShazamHash> hashes = new ArrayList<ShazamHash>();
        hashes = song_idx.combineHash();
        ArrayList<ArrayList<Integer>> data = new ArrayList<ArrayList<Integer>>();
        int size = StoreFingerprint.getSongCounts();
        for (int i = 0; i < size; i++) {
            ArrayList<Integer> i_list = new ArrayList<Integer>();
            data.add(i_list);
        }
        return hashes;
    }
    public static ArrayList<ArrayList<Integer>> getFingerPrint( ArrayList<ShazamHash> hashes){


        ArrayList<ArrayList<Integer>> data = new ArrayList<ArrayList<Integer>>();
        int size = StoreFingerprint.getSongCounts();
        for (int i = 0; i < size; i++) {
            ArrayList<Integer> i_list = new ArrayList<Integer>();
            data.add(i_list);
        }
        return data;
    }

}
