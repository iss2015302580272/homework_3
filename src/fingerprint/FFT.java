package fingerprint;

/**
 * Created by Alison on 2016/11/5.
 */


public class FFT {

    public static final int WINDOW_SIZE = 4096;

    //Bit Reverse
    private static void bit_reverse(fingerprint.Complex[] y) {
        int i, j, k;
        for (i = 1, j = y.length / 2; i < y.length - 1; ++i) {
            if (i < j) {
                fingerprint.Complex temp = y[i];
                y[i] = y[j];
                y[j] = temp;
            }
            k = y.length / 2;
            while (j >= k) {
                j -= k;            // eliminate leading 1.
                k /= 2;
                if (k == 0)        // cutting branches.
                    break;
            }
            if (j < k)             // add leading 1.
                j += k;
        }
    }
    private static fingerprint.Complex[] fft(Complex[] y) {
        bit_reverse(y);
        int j, k, h;

        // get the 1/8 part of data
        for (h = 2; h <= WINDOW_SIZE/8; h <<= 1) {
            // twiddle factor
            fingerprint.Complex omega_n = new fingerprint.Complex(Math.cos(-2 * Math.PI / h), Math.sin(-2 * Math.PI / h));
            for (j = 0; j < WINDOW_SIZE/8; j += h) {
                fingerprint.Complex omega = new fingerprint.Complex(1, 0);
                // butterfly transformation
                for (k = j; k < j + h / 2; ++k) {
                    fingerprint.Complex u = y[k];
                    fingerprint.Complex t = omega.mul(y[k + h / 2]);
                    y[k] = u.add(t);
                    y[k + h / 2] = u.sub(t);
                    omega = omega.mul(omega_n);
                }
            }
        }
        return y;
    }

    public static double[] fft(double[] slice) {

        if (slice.length != WINDOW_SIZE)
            throw new RuntimeException("FFT::fft(double[] slice) - " +
                    "The window size is not equal to the required window size (" + WINDOW_SIZE + ")");

        fingerprint.Complex[] x = new Complex[WINDOW_SIZE];

        /**
         * Convert the time-domain series as Complex series whose imaginary parts are zeros.
         */
        for (int i = 0; i < WINDOW_SIZE; ++i) {
            x[i] = new fingerprint.Complex(slice[i], 0);
        }

        fingerprint.Complex[] res = fft(x);

        double[] ret = new double[WINDOW_SIZE];
        for (int i = 0; i < WINDOW_SIZE; ++i) {
            /**
             * The magnitude of each frequency.
             */
            ret[i] = res[i].abs();
        }
        return ret;
    }

}