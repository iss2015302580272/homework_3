package grade;
/**
 * Created by Alison on 2016/11/15.
 */

import database.Print;
import database.StoreFingerprint;
import fingerprint.GetFingerPrint;
import fingerprint.ShazamHash;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.*;


public class EnquirySong {

    static final String songName = "周杰伦 - 说好的幸福呢_03.wav";
    static final String path = "data/wav_part_5/";

    public static void main(String[] args) throws IOException {
        long startTime = System.currentTimeMillis();   //get the start time

        //hashes that
        ArrayList<ShazamHash> hashes = GetFingerPrint.getHashes(path, songName);
        //data stored every song's maximum frequency
        ArrayList<ArrayList<Integer>> data = GetFingerPrint.getFingerPrint(hashes);

        System.out.println("Enquiry:  " + songName + "\n");

        //generate repeated data map
        Grade.Grade(hashes, data);

        MostNum most = new MostNum();
        Map map = new HashMap();
        int size = StoreFingerprint.getSongCounts();

        for (int i = 0; i < size; i++) {
            int max = most.method3(data.get(i));
            map.put(i + 1, max);
        }

        map = sortByValue(map);
        Set<Integer> key = map.keySet();
        List<Integer> finalID = new ArrayList<Integer>();
        for (Iterator it = key.iterator(); it.hasNext(); ) {
            Integer s = (Integer) it.next();
            finalID.add(s);
        }

        Print.PrintResult(finalID);

        long endTime = System.currentTimeMillis();
        System.out.println("\n" + "Time： " + (endTime - startTime) / 1000.0 + "s");
        Date today = new Date();
        System.out.println("Current time: " + new Timestamp(today.getTime()) + "\n");
    }


    //sort numbers in the order of value
    public static Map sortByValue(Map map) {
        List list = new LinkedList(map.entrySet());
        Collections.sort(list, new Comparator() {
            // 将链表按照值得从小到大进行排序
            public int compare(Object o1, Object o2) {
                return ((Comparable) ((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue());
            }
        });
        Map result = new LinkedHashMap();
        for (Iterator it = list.iterator(); it.hasNext(); ) {
            Map.Entry entry = (Map.Entry) it.next();
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }


}
